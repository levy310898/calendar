import React, { Suspense } from 'react';
import { BrowserRouter, Route, Switch,Redirect } from 'react-router-dom';
import './App.scss';
import NotFound from 'components/NotFound';
import Header from 'components/Header';
import LoadingPage from 'components/LoadingPage';
const Calendar = React.lazy(() => import('./features/Calendar/index'))
function App() {
  return (
    <div className="App">
      <Suspense fallback={<LoadingPage/>}>
        <BrowserRouter>
          <Header />
          <Switch>
            <Redirect exact from='/' to='/calendar' />
            <Route path='/calendar' component={Calendar} />
            <Route component={NotFound} />
          </Switch>
        </BrowserRouter>
      </Suspense>
    </div>
  );
}

export default App;
