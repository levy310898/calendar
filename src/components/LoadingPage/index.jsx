import React from 'react'
import { Spin } from 'antd';
import { flexibleCompare } from '@fullcalendar/react';
export default function index() {
  return (
    <div style = {{display:'flex',alignItems:'center',justifyContent:'center'}}>
      <Spin size='large' />
    </div>
  )
}
