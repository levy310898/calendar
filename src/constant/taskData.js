const task = [
  {
    id: 6,
    title: 'some task you need 6',
    description: 'you need to do this or ...',
    date: "2021-07-21T04:36:18.622Z",
    startDate: "2021-07-21T11:00:14.113Z",
    endDate: "2021-07-21T12:00:14.114Z",
  },
  {
    id: 8,
    title: 'some task you need 8',
    description: 'you need to do this or ...',
    date: "2021-07-28T04:36:18.622Z",
    startDate: "2021-07-28T07:30:00",
    endDate: "2021-07-28T11:30:00",
  },
  {
    id: 9,
    title: 'some task you need 9',
    description: 'you need to do this or ...',
    date: "2021-07-14T04:36:18.622Z",
    startDate: "2021-07-14T10:30:00",
    endDate: "2021-07-14T11:30:00",
  },
  {
    id: 10,
    title: 'some task you need 10',
    description: 'you need to do this or ...',
    date: "2021-07-02T04:36:18.622Z",
    startDate: "2021-07-14T12:30:00",
    endDate: "2021-07-14T13:30:00",
  },
]

export default task;