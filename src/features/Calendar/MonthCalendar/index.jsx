import React,{useEffect,useState,useRef} from 'react';
import moment from 'moment';
import './MonthCalendar.scss';
import taskData from 'constant/taskData';
import FullCalendar from '@fullcalendar/react' // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid' // a plugin!
import interactionPlugin from "@fullcalendar/interaction" // needed for dayClick
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import ModalEditTask from '../components/ModalEditTask';
import ButtonAddTask from '../components/ButtonAddTask';

export default function MonthCalendar() {
  const [task, setTask] = useState([]);
  const modalRef = useRef();
  useEffect(() => {
    setTask(taskData);
    console.log(task);
  },[])

  const handleDateClick = (e) => {
    modalRef.current.onShow({date:moment(e.date)});
  }

  const handleEventClick = (e) => {
    // console.log("id = ", e.event.id, ", task = ", );
    modalRef.current.onShow({ item: task.find(item => item.id == e.event.id) });
  }

  const renderEventContent = (eventInfo) => {
    return (
      <p>
        <b>{moment(eventInfo.event.start).format('HH:mm')}</b> <i>{eventInfo.event.title}</i>
      </p>
    )
  }

  return (
    
    <>
      <FullCalendar
        plugins={[dayGridPlugin, interactionPlugin, timeGridPlugin, listPlugin]}
        initialView="dayGridMonth"
        events={
          task.map(item => ({
            ...item,
            date: moment(item.date).format(),
            start: moment(item.startDate).format(),
            end: moment(item.endDate).format(),
          })
          )}

        headerToolbar={{
          start: 'prev,next',
          center: 'title',
          end: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
        }}

        eventContent={renderEventContent}
        dateClick={(date) => handleDateClick(date)}
        eventClick={(date) => handleEventClick(date)}
      />

      <ModalEditTask ref={modalRef} />
      
      <ButtonAddTask handleClick={() => modalRef.current.onShow({})}/>
    </>
    
  )
}


