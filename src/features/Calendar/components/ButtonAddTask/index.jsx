import React from 'react'
import { Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import './index.scss';
export default function ButtonAddTask(props) {
  const { handleClick } = props;
  return (
    <Button type="primary" shape="circle" onClick={handleClick} size = 'large' className = "button__add-task">
      <PlusOutlined />
    </Button>
  )
}
