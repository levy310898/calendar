import React, { Component } from 'react';
import { Modal, Form, Input, DatePicker,TimePicker,message} from 'antd';
import moment from 'moment';
import { ClockCircleOutlined } from '@ant-design/icons';


class ModalEditTask extends Component {
  formRef = React.createRef();
  state = {
    isVisible: false,
  }

  getISODate = date => date.toISOString();

  onHandleOk = () => {
    const validation = this.formRef.current.validateFields();
    validation
      .then(() => {
        const { title, date, time } = this.formRef.current.getFieldsValue();
        const collection = {
          title,
          date,
          startDate: this.getISODate(time[0]),
          endDate: this.getISODate(time[1]),
        }
        console.log("collection = ", collection);
        if (this.state.id) {
          console.log('update task ',this.state.id);
        } else {
          console.log('create new task');
        }
        this.setState({ isVisible: false })
      })
      .catch((err) => {
        message.error("some err");
      })
  }

  onHandleCancel = () => {
    this.setState({ isVisible: false });
  }

  handleRoundTime = time => {
    const remainder = 15 - (time.minute() % 15);
    const dateTime = moment(time).add(remainder, "minutes");
    return dateTime;
  }

  onShow = ({ date=null, item={} }) => {
    
    // const { id, title, dateItem, startDate, endDate } = item?item : {id:null,title:'',dateItem:null,startDate:null,endDate:null};
    const { id = null, title = "", dateItem = null, startDate = null, endDate = null } = item;
    
    const nowTimeRound = this.handleRoundTime(moment());
    const endTimeRound = this.handleRoundTime(moment().add(1,'hours'));
    this.setState({ id, isVisible: true }, () => {
      this.formRef.current.setFieldsValue({
        date: date ? date : dateItem ? moment(dateItem) : moment(),
        title,
        time:startDate && endDate ? [moment(startDate),moment(endDate)]:[nowTimeRound, endTimeRound],
    })
    })
  }
  
  render() {
    return (
      <Modal  title = "Add task" visible={this.state.isVisible} onCancel={this.onHandleCancel} onOk={this.onHandleOk}>
        <Form
          ref={this.formRef}
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 16 }}
        >

          <Form.Item
            name="title"
            label="Tiêu đề"
            rules={[
              {
                required: true,
                message:'missing time !!!',
              },
            ]}
          >
            <Input placeholder="Nhập tiêu đề" />
          </Form.Item>

          
          <Form.Item
            name="date"
            label={<ClockCircleOutlined />}
            rules={[
              {
                required: true,
                message: 'missing date !!!',
              },
            ]}
          >
            <DatePicker />
          </Form.Item>

{/* 
          <Form.Item
            name="startDate"
            label="start"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <TimePicker format="HH:mm" minuteStep={15}/>
          </Form.Item> */}

          <Form.Item
            name="time"
            label="time"
            rules={[
              {
                required: true,
                message: 'missing time !!!',
              },
            ]}
          >
            <TimePicker.RangePicker format="HH:mm" minuteStep={15} order={false}/>
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}

export default ModalEditTask;
