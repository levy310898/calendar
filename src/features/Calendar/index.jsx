import React from 'react';
import { Route, Switch,Redirect } from 'react-router-dom';
import { useRouteMatch } from 'react-router-dom';
import MonthCalendar from './MonthCalendar';
import YearCalendar from './YearCalendar';
import NotFound from 'components/NotFound';

export default function Index(props) {
  const match = useRouteMatch();
  return (
    <Switch>
      <Redirect exact from={match.url} to={`${match.url}/month`} />
      <Route exact path={`${match.url}/month`} component={MonthCalendar} />
      <Route exact path={`${match.url}/year`} component={YearCalendar} />
      <Route component={NotFound} />
    </Switch>
  )
}
